class CreateCentros < ActiveRecord::Migration[5.0]
  def change
    create_table :centros do |t|
      t.string :nombre
      t.string :direccion
      t.string :email

      t.timestamps
    end
  end
end
