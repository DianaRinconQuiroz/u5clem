class AddAvatarToAlumnos < ActiveRecord::Migration[5.0]
  def change
    add_column :alumnos, :avatar, :string
  end
end
