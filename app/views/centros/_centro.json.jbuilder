json.extract! centro, :id, :nombre, :direccion, :email, :created_at, :updated_at
json.url centro_url(centro, format: :json)
