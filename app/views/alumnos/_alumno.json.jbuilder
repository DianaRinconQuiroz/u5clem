json.extract! alumno, :id, :nombre, :apellido, :edad, :telefono, :centro_id, :created_at, :updated_at
json.url alumno_url(alumno, format: :json)
