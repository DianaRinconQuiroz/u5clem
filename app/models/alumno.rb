class Alumno < ApplicationRecord
    belongs_to :centro
    mount_uploader :avatar, FotoUploader
end
